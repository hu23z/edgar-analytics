selection = ['ip', 'date', 'time', 'cik', 'accession', 'extention']

elapse = 0
coll = {}  # configure dictionary
output = []


class Session:
    def __init__(self, ip, start_time):
        self.ip = ip
        self.first_time = start_time
        self.last_accessed = start_time
        self.count = 1
        # self.count = count

    def update(self, t):
        if not self.is_avail(t):
            return  # security check
        self.last_accessed = t  # update the most recent accessed time
        self.count += 1  # increase the number of documents accessed by one as default

    def is_avail(self, t):
        if (
                t - self.last_accessed) > elapse:  # if certain seconds of inactivity have passed for this session
            return False  # close the session
        return True

    def toString(self):
        duration = (self.last_accessed - self.first_time).total_seconds() + 1  # inclusive
        import datetime
        out = ','.join([self.ip,  # IP address
                        datetime.datetime.strftime(self.first_time, '%Y-%m-%d %H:%M:%S'),
                        # date time of the first request in the session
                        datetime.datetime.strftime(self.last_accessed, '%Y-%m-%d %H:%M:%S'),
                        # date time of the last request in the session
                        str(int(duration)),  # duration of the session in seconds
                        str(self.count)])  # count of requests during the session
        return out + '\n'


def get_elapse(path):
    """

    @param path:
    @return:
    """
    with open(path, 'r') as file:
        str = file.readline()  # reads a single line from the file
    ela = int(str)
    if ela < 1:
        raise ValueError('Invalid Inactivity period. '
                         'Inactivity period must be at least 1 second')
    elif ela > 86400:
        raise ValueError('Invalid Inactivity period. '
                         'Inactivity period cannot be greater than 86,400 seconds (24h)')
    # range from 1 to 86, 400(i.e., one second to 24 hours)
    return int(str)


def update_elapse(n):
    """

    @param n:
    """
    global elapse
    elapse = n


def is_valid_file(path):
    """

    @param path:
    """
    import os
    if not os.path.isfile(path):
        import argparse
        raise argparse.ArgumentTypeError('The file %s does not exist' % format(os.path.normpath(path)))


def get_parser():
    """

    @return:
    """
    from argparse import ArgumentParser, FileType
    parser = ArgumentParser()
    # If no command-line arg is present, the value from default will be produced
    parser.add_argument('log',
                        help='read log.csv in input folder',
                        nargs='?', default='log.csv')
    parser.add_argument('elapse',
                        help='read inactivity_period.txt in input folder',
                        nargs='?', default='inactivity_period.txt', )

    parser.add_argument('output', action='store', nargs='?', default='./output/sessionization.txt',
                        type=FileType('w'),
                        help='Directs the output to a name assigned or the default')

    args = parser.parse_args()

    # if using absolute path
    # import os
    # curr_path = os.getcwd() # current working directory
    # import sys
    # script_path = os.path.dirname(os.path.realpath(sys.argv[0]))
    # log_path = os.path.join(curr_path, args.log)
    # elapse_path = os.path.join(curr_path, args.elapse)

    is_valid_file(args.log)

    from datetime import timedelta
    elapse = timedelta(
        seconds=get_elapse(args.elapse))  # a single integer value denoting the period of inactivity(in seconds)
    update_elapse(elapse)  # update global variable
    return args.output, parse_logs(args.log)


def parse_logs(path):
    """

    @param path:
    @return:
    """
    import csv
    inactive_str = ''
    active_str = ''
    with open(path, 'r') as f:
        reader = csv.DictReader(f)
        # headers = reader.fieldnames
        for line in reader:
            parse_request(line)
    for _, i in sorted(output, key=lambda x: x[0]):  # sorted by first_time
        inactive_str += i
    temp = []
    for i in coll:
        s = coll[i]
        temp.append((s.first_time, s.toString()))
    for _, i in sorted(temp, key=lambda x: x[0]):
        active_str += i
    return inactive_str + active_str


def parse_request(line):
    """

    @param line:
    """
    from datetime import datetime
    ip = line['ip']
    time_str = ' '.join((line['date'], line['time']))
    timestamp = datetime.strptime(time_str, '%Y-%m-%d %H:%M:%S')
    # print(timestamp)

    for key in list(coll.keys()):
        if not coll[key].is_avail(timestamp):
            output.append((coll[key].first_time, coll[key].toString()))
            # print('delete %s' % key)
            del coll[key]
    # by the time all expired session were moved to finished list
    if ip in coll:  # if not first-time user
        coll[ip].update(timestamp)  # returns false if the session is closed
    else:
        coll[ip] = Session(ip, timestamp)
    # cik = line['cik']
    # print('cik %s' % cik)
    # accession = line['accession']
    # print('accession %s' % accession)
    # extention = line['extention']
    # print('extention %s' % extention)


def write_to_file(path, string):
    """

    @param path:
    @param string:
    """
    path.write(string)


if __name__ == '__main__':
    output_path, output = get_parser()
    write_to_file(output_path, output)
