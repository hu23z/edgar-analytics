# Edgar Analytics

## Summary

This is a solution to a coding [challenge](https://github.com/InsightDataScience/edgar-analytics) for Insight Data Engineering program.

It parses EDGAR weblogs in the csv format, and outputs records of user sessions to a txt file.

My approach defines a `session` class object to represent a user session, holding the fields such as ip, start time, last accessed time, and some relevant methods, such as updating the counts of documents visited, checking if certain seconds of inactivity have passed for this session.

The programs keeps a dictionary of sessions, and a list of strings that describes the sessions. When parsing each line request, the program first checks if the active sessions stored in the dictionary are still valid, if not, they will be appended to the output list and deleted from the dictionary. Then, if will check if the ip is a first time user for still active sessions, and decides if a new session should be created. After all, it will write strings from the output list to the txt file. The file will be stored in the output folder.


## Dependencies

### Language: Python 3.6
### Modules
* `os`: check if the desired file exists in the directories
* `argparse`: figures out what arguments to expect and helps save them as variables to be used
* `csv`: reads data in dictionary form using the `DictReader`
* `datetime`: manipulates dates and times, such as `strftime()` and `strptime()` behaviors


## Instructions

Locate to the folder where `run.sh` exists, run the shell script, find the output file named `sessionization.txt` in the output folder.
